const express = require('express');
const bodyParser = require('body-parser');

var app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: true
}));

app.use('/weather', require('./routes/weather'));


app.listen(3000);

console.log('Running on port 3000');